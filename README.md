# OpenStack Horizon creation of virtual server
### In this repo I will demonstrate how to setup a VM in Openstack using the Horizon dashboard graphic user interface (GUI).


## Getting started

**In this example I will use an ubuntu image and restrict SSH access.**

- login to your account using the horizon dashboard. <br/>
    ![Login](./images/login.png)



## 1. Create Security Groups.
here we will create roles. Open ports for different protocols that must have a certain role.
***the purpose of this is to open three ports one for ping and for web server and for ssh connecting***
- From the left list select Project > Network > Security Groups.
- select +create security group. <br/>
    ![Image](./images/Security Groups.png)
- here write Name and Description for Security Group. <br/>
    ![Image](./images/Security Groups2.png)
- now we will make rolls and open ports, select manage rulls for your secutity group name . <br/>
    ![Image](./images/manage rulls.png)   
- and in this page select +add rulls. <br/>
    ![Image](./images/add rulls.png)
- here you choose the three rolls that we mentioned at the beginning.
    1. **Ping**. In Ruls select > **All ICMP**, Direction > Ingress, Remote > CIDR, CIDR > 0.0.0.0/0 and press Add   <br/>
       ![Image](./images/add rulls2.png) <br/>
       **Do the same with 2 and 3**
    2. **web sever**. In Ruls select > **HTTP**, Direction > Ingress, Remote > CIDR, CIDR > 0.0.0.0/0 and press Add.
    3. **ssh connecting**. In Ruls select > **SSH**, Direction > Ingress, Remote > CIDR, CIDR > 0.0.0.0/0 and press Add.

## 2. Create SSH Key(Key Pairs).
Now we have a port open for SSH. So we will make an ssh key for the connection to VM.
- From the left list select projects > Compute > Key Pairs and celect +create key pair and make your ssh key and save the file that has created in a folder so you can use it.

## 3. Create Network and Subnet.
Now we will create a network so that our VM can be accessible out there on the internet.
- From the left list select Project > Network > Network Topology and choose +create network.
- Enter network name, and as well check Enable Admin State,Create Subnet.  <br/>
    ![Image](./images/create network.png)
- On the same box that you are currently select Subnet.
    1. Enter subnet name.
    2. Network Address. For me this its work(192.168.1.0/24)it depends on your router.if that doesn't work try this(192.168.0.0/24).
    3. Gateway IP. for me this one 192.168.1.1 its work.
    4. uncheck Disable Gateway. <br/>
        ![Image](./images/create_network2.png)
- On the same box that you are currently select Subnet Details.
    1. Check Enable DHCP.
    2. DNS Name Servers. On the box write this 8.8.8.8 so you can from your server ping DNS address out on the internet.
    3. Press button create. <br/>
       ![Image](./images/create_network4.png)
   
 

## 4. Create Router.
Now we will create a Router so that our VM-network internt that have been created can be accessible out there on the internet By The Router.
- From the left list select Project > Network > Routers and choose +create router. <br/>
    ![Image](./images/router.png)
1. Enter router name.
2. check Enable Admin State.
3. External Network select public(in our case elx-public1) and press button create. <br/>
    ![Image](./images/router2.png)
 **Now we have to connect the router to the vm-network.** <br/> 
    ![Image](./images/router3.png)
4. Select Network Topology and choose Graph and press on your router. <br/>
    ![Image](./images/router4.png)
5. select +add interface. <br/>  
    ![Image](./images/router5.png)
6. select your subnet that you have been created, and IP Address (Your Gateway IP) and press submit. <br/> 
    ![Image](./images/router6.png) <br/>
**Now You should have like this image** <br/>
    ![Image](./images/router7.png)

## 5. Create Your Instances(VM).
- From the left list select Project > Compute > Instances
1. Select Lunch instance.
2. Enter your Instance Name and press Next. <br/>
    ![Image](./images/instance.png)
3. Select Boot Source, Volume Size (GB) , Allocated (Your image) from the list Available,Create New Volume,Delete Volume on Instance Delete and press Next. <br/>
    ![Image](./images/instance1.png)
4. Select your Flavor,from the list Available and press Next. <br/> 
    ![Image](./images/instance2.png)
5. Select your network that you have been created early. <br/>
    ![Image](./images/instance3.png)
6. Select the security groups that you have been created early to launch the instance in. <br/>
    ![Image](./images/instance4.png)
7. Select your key pair that you have been created.
8. Select Launch Instance.

## 5. Access the machine from the internet.
- We Need to add a public internet and mapping the machine to the internet.
1. under **Action** column select Associat Floating IP.
2. Select the IP address you wish to associate with the selected instance. <br/>
    ![Image](./images/floating.png)
3. Allocate a floating IP from a given floating IP pool so choose your public one and press Allocate IP. <br/>
    ![Image](./images/floating1.png)
4. Now you can se you get a public ip address for your machine press Associate. <br/>
    ![Image](./images/floating2.png) <br/>
** Now Your machine it's Live**
